/**
 * Created by eralp on 17/06/2017.
 */


export default class {
    _name: string
    /*
     constructor(public name: string) {

     }*/
    constructor(name: string) {
        this._name = name
    }

    showMyName(): string {
        return this._name
    }

    get MyName():string {
        return this._name;
    }

    set MyName(val:string) {
        this._name = val
    }
}