"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var second_1 = require("./second");
/**
 * Created by eralp on 17/06/2017.
 */
function ShowValue(a) {
    return a;
}
function ShowValueV2(a, myT) {
    return a;
}
//noinspection JSUnusedGlobalSymbols
//let f:number = ShowValue(1)
//let f1:string = ShowValue(1)
function dummy() {
    var s = new second_1.second();
    //let f: any = ShowValueV2<second>(s);
    var f = ShowValueV2(s, [1, new Object()]);
    var f2 = ShowValueV2(1, [1, new Object()]);
    var f3 = new gigi();
    var f4 = new gigi();
}
var gigi = (function () {
    function gigi() {
    }
    gigi.prototype.ShowMe = function (t) {
        return t;
    };
    return gigi;
}());
//# sourceMappingURL=genericsample.js.map