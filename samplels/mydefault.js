"use strict";
/**
 * Created by eralp on 17/06/2017.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var default_1 = (function () {
    /*
     constructor(public name: string) {

     }*/
    function default_1(name) {
        this._name = name;
    }
    default_1.prototype.showMyName = function () {
        return this._name;
    };
    Object.defineProperty(default_1.prototype, "MyName", {
        get: function () {
            return this._name;
        },
        set: function (val) {
            this._name = val;
        },
        enumerable: true,
        configurable: true
    });
    return default_1;
}());
exports.default = default_1;
//# sourceMappingURL=mydefault.js.map