import {second} from "./second";
/**
 * Created by eralp on 17/06/2017.
 */


function ShowValue(a: string | number): number | string {
    return a
}

function ShowValueV2<T extends second, Y, Z>(a: T, myT: [Y, Z]): T {
    return a
}

//noinspection JSUnusedGlobalSymbols
//let f:number = ShowValue(1)
//let f1:string = ShowValue(1)
function dummy() {
    let s: second = new second()
    //let f: any = ShowValueV2<second>(s);
    let f: second = ShowValueV2<second, number, any>(s, [1, new Object()]);
    let f2: number = ShowValueV2<number, number, any>(1, [1, new Object()]);

    let f3 : gigi<number>=new gigi<number>()
    let f4:ITest<number>= new gigi<number>()

}

interface ITest<T extends string|number> {
    ShowMe: (t: T) => T
}
class gigi<T extends string|number> implements ITest<T> {
    ShowMe(t: T): T {
        return t
    }
}


