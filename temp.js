"use strict";
///<reference path="another.ts"/>
/**
 * Created by eralp on 16/06/2017.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var temp = (function () {
    //lastName: string;
    //name: string;
    //constructor(public name: string) {
    //}
    //constructor(_name: string = "value") {
    //    this.name = _name
    //}
    /*
     constructor(public lastName:string) {
     let a: string[];
     let a4: Array<string>
     let a1 = [];
     let a3: any[] = [1, 2, new tempy("222")];
     a1.push();
     let a2 = []
     }
     */
    function temp(name) {
        this.name = name;
        this.samplez("100");
    }
    temp.prototype.samplez = function (n1, n2) {
        if (n1 === void 0) { n1 = "aa"; }
        if (n2 === void 0) { n2 = "aa"; }
    };
    temp.prototype.ShowAllValues = function () {
        var values = [];
        for (var _a = 0; _a < arguments.length; _a++) {
            values[_a] = arguments[_a];
        }
        values.forEach(function (d, index) {
            console.log(d + "  ,\n                    temp text info             \n             " + index);
        });
    };
    temp.prototype.TempLoad = function () {
        var tempList;
        tempList.push(new temp("s"), new temp("dd"), { name: "tempx" });
        return tempList;
    };
    temp.prototype.TempLoad2 = function () {
        var tempList = new Array();
        tempList.push(new temp("ee"), new temp("www"), { name: "tempx" });
        return tempList;
    };
    /*
     public emit<T1>(event: string, arg1: T1): void {

     }
     public emit<T1,T2>(event: string, arg1: T1, arg2: T2): void {

     }
     public emit(event: string, ...args: any[]): void {
     // actual implementation here
     }

     temp(a:boolean):number[] {
     return []
     }

     temp(a: string, b: string):void {

     }
     */
    temp.showme = function (t) {
        if (t === void 0) { t = Tempx.B; }
        console.log("temp" + ", " + t.toString());
    };
    return temp;
}());
exports.temp = temp;
var xx;
//Elmax.KucukElma.IX
var ffdd = Math.pow(2, 5); // 32!
console.log(ffdd);
var Person = (function () {
    function Person(id, name) {
        this.id = id;
        this.name = name;
    }
    Person.prototype.GetInfo = function () {
        return [this.id.toString(), this.name];
    };
    return Person;
}());
var PersonUtility = (function () {
    function PersonUtility() {
    }
    PersonUtility.prototype.GetAllPeople = function () {
        var pList = [
            new Person(1, "Ali"),
            new Person(2, "Veli"),
            new Person(3, "Ceyhun"),
        ];
        return pList;
    };
    PersonUtility.prototype.GetPersonById = function (i) {
        var result = this.GetAllPeople().filter(function (u) {
            return u.id == i;
        });
        if (result.length == 1) {
            return result[0];
        }
        else {
            return null;
        }
    };
    PersonUtility.prototype.Every = function () {
        var result = this.GetAllPeople().every(function (x) {
            return x.id > 0;
        });
        return result;
    };
    PersonUtility.prototype.ShowAllPersonInformations = function () {
        this.GetAllPeople().forEach(function (y) {
            console.log(y.GetInfo());
        });
    };
    PersonUtility.prototype.ShowEmptyRow = function () {
        this.GetAllPeople().forEach(function () {
            console.log("**");
        });
    };
    return PersonUtility;
}());
var tempNew = (function () {
    function tempNew() {
    }
    return tempNew;
}());
exports.tempNew = tempNew;
var g = (function () {
    function g() {
    }
    g.prototype.elma = function (t) {
        return 10;
    };
    g.prototype.complex = function (h) {
        var x = Array();
        //
        return x;
    };
    return g;
}());
var xxx = (function () {
    function xxx() {
    }
    xxx.prototype.elma = function (a) {
    };
    return xxx;
}());
var applex = (function () {
    function applex() {
    }
    applex.prototype.elma = function () {
        return "";
    };
    return applex;
}());
var applexx = (function (_super) {
    __extends(applexx, _super);
    function applexx() {
        return _super.call(this) || this;
    }
    applexx.prototype.elma = function () {
        return _super.prototype.elma.call(this);
    };
    return applexx;
}(applex));
var fz = (function () {
    function fz(tempInfo) {
        //var x= new applex()
        //x.elma()
        new applexx().elma();
        this.tempInfo = tempInfo;
    }
    Object.defineProperty(fz.prototype, "tempInfoGet", {
        get: function () {
            return this.tempInfo;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(fz.prototype, "tempInfoSet", {
        set: function (value) {
            this.tempInfo = value;
        },
        enumerable: true,
        configurable: true
    });
    return fz;
}());
var gd = (function () {
    function gd() {
    }
    gd.prototype.elma2 = function (s) {
        return 10;
    };
    return gd;
}());
var gextend = (function (_super) {
    __extends(gextend, _super);
    function gextend() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    gextend.prototype.elma = function (n) {
        throw new Error("Method not implemented.");
    };
    return gextend;
}(gd));
var a = (function () {
    function a() {
        //var ff = new gd()
    }
    return a;
}());
var b = (function (_super) {
    __extends(b, _super);
    function b() {
        return _super.call(this) || this;
    }
    return b;
}(a));
var samplexz = (function () {
    function samplexz() {
    }
    samplexz.elma = function () {
        return "";
    };
    return samplexz;
}());
//let xxxInfo = new samplexz().elma()
//samplexz.elma()
var allz = (function () {
    function allz(item) {
        this.item = item;
        this._item = item;
    }
    allz.prototype.show = function () {
        console.log(this._item.lastname + " ," +
            this._item.id + " , " + this._item.name);
    };
    return allz;
}());
var fg = {
    name: "eralp",
    lastname: "tt",
    id: 2000
};
(new allz(fg)).show();
var tempxy = (function (_super) {
    __extends(tempxy, _super);
    function tempxy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return tempxy;
}(tempNew));
exports.tempxy = tempxy;
var probablyADuck = {
    walk: function () { return console.log('walking like a duck'); },
    swim: function () { return console.log('swimming like a duck'); },
    quack: function () { return console.log('quacking like a duck'); }
};
function FlyOverWater(bird) {
    bird.quack();
}
//FlyOverWater(probablyADuck); // works!!!
console.log("Number --->", (new g()).elma("ewewew"));
var tempy = (function () {
    function tempy(id) {
        this.id = id;
        this.id = "10";
        var val1 = id;
        var val2 = id;
        var x11 = [id, val1];
        var x12 = [id, val2];
        console.log(x11.toString(), x12.toString());
    }
    return tempy;
}());
//var tt = new tempy("e33434")
var Tempx;
(function (Tempx) {
    Tempx[Tempx["A"] = 100] = "A";
    Tempx[Tempx["B"] = 101] = "B";
})(Tempx || (Tempx = {}));
function t() {
}
function t1(a, b) {
}
function GenerateKey() {
    var _i = 100;
    return function () {
        _i += 1;
        return _i;
    };
}
//var f1 = new t1(10, 10, 10)
//var t = new temp("temp");
//var t = new temp();
//t.name = "temp";
//temp.showme();
/*
 var f = () => {

 }
 f.prototype.temp = function () {

 }
 */
/*
 (function SampleOne() {
 for (var i = 0; i < 1000000; i++) {
 setTimeout(function () {
 console.log("Value of i :", i)
 }, i * 100)
 }
 })()
 */
/*
 (function SampleTwo() {
 for (let i = 0; i < 10; i++) {
 setTimeout(function () {
 console.log("Value of i :", i)
 }, i * 100)
 }
 })()
 */
/*
 (function SampleTwo() {
 for (var i = 0; i < 10; i++) {
 (function (val) {
 setTimeout(function () {
 console.log("Value of i :", val)
 }, val * 100)
 })(i)
 }
 })()

 */
(function () {
    //var valFunction = GenerateKey()
    //console.log(GenerateKey()())
    //for (var r = 0; r < 10; r++) {
    //    console.log(valFunction())
    //}
})();
